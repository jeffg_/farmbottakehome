/*** 
 * Finds the index with the maximum subarray sum given the sub array length
 * @param {number[]} array - An array
 * @param {number} arrayLen - Length of the array
 * @param {number} subArrayLength - Desired Length of the SubArray 
 * @returns {number} The index at which the subarray sum of length SubArrayLength is maximum or -1 if invalid
***/
function calc(array: number[], arrayLen: number, subArrayLength: number) : number {
     // Utilise BigInt to prevent integer overflow
    let windowSum : bigint = 0n;
    let windowStartIndex : number = 0;
    let maxWindowSum : bigint;
    let maxWindowStartIndex : number = 0;
    let i : number = 0;

    /* 
        If SubArrayLength is greater than arrayLen this is an invalid input
        We can either truncate or subArrayLength to arrayLen or return -1 to signal an error
         quiet failures are dangerous and can hide unintentional behaviour, it is better to return -1 or even throw
    */
    if (subArrayLength > arrayLen || subArrayLength < 0)
        return -1;

    // Simple Sliding Window
    for (i = 0; i < subArrayLength; i++) 
        windowSum = windowSum + BigInt(array[i]);
    maxWindowSum = windowSum;
    
    for (i = subArrayLength; i < arrayLen; i++) {
        windowSum = windowSum + BigInt(array[i] - array[i - subArrayLength]);
        windowStartIndex++;
        if (maxWindowSum < windowSum) {
            maxWindowSum = windowSum;
            maxWindowStartIndex = windowStartIndex;
        }
    }
    
    return maxWindowStartIndex;
}

export default calc;