import calc from '.';

interface testType {
    arr : number[],
    subArrSize: number,
    output: number
}

it('Works for a basic scenarios', () => {
    const scenarios : testType[] = [
        {arr: [10,15,20,25], subArrSize: 2, output: 2}, // Incrementing
        {arr: [25,20,15,10], subArrSize: 2, output: 0},  // Decrementing
        {arr: [20,25,40,40,20], subArrSize: 2, output: 2}, 
        {arr: [0,0,0,0,0,0], subArrSize: 2, output: 0}, // Zeroes
        {arr: [-6,-2,-3,-4,-5,-6], subArrSize: 2, output: 1}, // Negatives
    ];
    
    for (let {arr, subArrSize, output} of scenarios) {
        expect(calc(arr, arr.length, subArrSize)).toBe(output)
    }
});

it('Handles an empty array', () => {
    const arr : number[] = [];
    expect(calc(arr, arr.length, 0)).toBe(0);
});


it ('Correctly handles different sizes of subArrayLengths', () => {
    const scenario = [10,20,30,40,50];
    const calcWithScenario = (l: number) => calc(scenario, scenario.length, l); 

    expect(calcWithScenario(1)).toBe(4);
    expect(calcWithScenario(2)).toBe(3);
    expect(calcWithScenario(3)).toBe(2);
    expect(calcWithScenario(4)).toBe(1);
    expect(calcWithScenario(5)).toBe(0);
    expect(calcWithScenario(10)).toBe(-1); // Invalid SubArrayLength
    expect(calcWithScenario(-1)).toBe(-1); // Invalid SubArrayLength
});
     
it('Works with an array with large numbers', () => {
    const scenarioA : number[] = [0,0,0,Number.MAX_SAFE_INTEGER,Number.MAX_SAFE_INTEGER,Number.MAX_SAFE_INTEGER];
    const scenarioB : number[] = [0,0,0,Number.MAX_SAFE_INTEGER,Number.MIN_SAFE_INTEGER,Number.MAX_SAFE_INTEGER];
    const scenarioC : number[] = Array(1000).fill(Number.MAX_SAFE_INTEGER);
    scenarioC[0] = 0; // Offset by 1

    expect(calc(scenarioA, scenarioA.length, 3)).toBe(3);
    expect(calc(scenarioA, scenarioA.length, 2)).toBe(3);
    expect(calc(scenarioA, scenarioA.length, 1)).toBe(3);

    expect(calc(scenarioB, scenarioB.length, 3)).toBe(1);
    expect(calc(scenarioB, scenarioB.length, 2)).toBe(2);
    expect(calc(scenarioB, scenarioB.length, 5)).toBe(1);

    expect(calc(scenarioC, scenarioC.length, scenarioC.length-1)).toBe(1);
    scenarioC[0] = Number.MAX_SAFE_INTEGER;
    expect(calc(scenarioC, scenarioC.length, scenarioC.length-1)).toBe(0);
});
